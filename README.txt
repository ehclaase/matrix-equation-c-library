Overview 	: 	Pass a matrix equation as a string to the library function Meq, which will parse and interpret the string and set the output matrix.
				This library is developed to increase readability and to reduce implementation time of mathematical matrix equations in the C programming language.
				
Usage 		:	Include the source files in /Source in a C project, and use the function "Meq" to implement a matrix equation as written string text. 
				See the test source file /Test/MatrixEquationTest.c for examples on usage of function "Meq".
				Define the macro MEQVERBOSE in /Source/MatrixEquation.h to have the progress of the parser and interpreter printed to the screen. 
				Define the macro MEQDEBUG in /Source/MatrixEquation.h to get debugging information in the case that "Meq" causes your program to exit.
				See the comments at the top of Source/MatrixEquation.c for the matrix operators and constants available.

Licence 	:	This program is free software: you can redistribute it and/or modify
				it under the terms of the GNU General Public License as published by
				the Free Software Foundation, either version 3 of the License, or
				(at your option) any later version.

				This program is distributed in the hope that it will be useful,
				but WITHOUT ANY WARRANTY; without even the implied warranty of
				MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
				GNU General Public License for more details.

				You should have received a copy of the GNU General Public License
				along with this program.  If not, see <http://www.gnu.org/licenses/>.