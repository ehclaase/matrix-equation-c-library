/**
Author		: 	Etienne H. Claase
Date  		: 	20171201
Description	: 	This program is a math equation C library developed to increase readability, 
				and reduce implementation time, of mathamatical equation in the C programming language.
Use			:	Examples
					Rotational dynamics 
						Meq("DBwBI_B[3,1,0] = IBB_B[3,3,1]\(mB_B[3,1,2] - wBI_B[3,1,3]x(IBB_B[3,1,1]*wBI_B[3,1,3]))", DBwBI_B, IBB_B, mB_B, wBI_B);
						where 
							meqNum_t DBwBI_B[3][1];
							meqNum_t IBB_B[3][3];
							meqNum_t mB_B[3][1];
							meqNum_t wBI_B[3][1];
					Matrix exponential approximation
						Meq("ePowX[10,10,0] = E + X[10,10,1] + 0.5.*X[10,10,1]*X[10,10,1] + X[10,10,1]*X[10,10,1]*X[10,10,1]./6 + X[10,10,1]*X[10,10,1]*X[10,10,1]*X[10,10,1]./24",ePowX,X);
						where 
							meqNum_t ePowX[7][7];
							meqNum_t X[10][10];
				
				Only the values of the input argument corresponding to the output matrix will be overwritten. The remaining input arguments remains unchanged when Meq is called.
				Note that alternatively the output matrix (e.g. ePowX[10,10,0]) may refer to the same input argument as one of the inputs (e.g. X[10,10,1]),
				i.e. Meq("ePowX[10,10,0] = E + X[10,10,0] + 0.5.*X[10,10,0]*X[10,10,0] + X[10,10,0]*X[10,10,0]*X[10,10,0]./6 + X[10,10,0]*X[10,10,0]*X[10,10,0]*X[10,10,0]./24",X);
				in which case the input argument X will be overwritten with the result.
Licence 	:	This program is free software: you can redistribute it and/or modify
				it under the terms of the GNU General Public License as published by
				the Free Software Foundation, either version 3 of the License, or
				(at your option) any later version.

				This program is distributed in the hope that it will be useful,
				but WITHOUT ANY WARRANTY; without even the implied warranty of
				MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
				GNU General Public License for more details.

				You should have received a copy of the GNU General Public License
				along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef __MATRIXEQUATION_H__
#define __MATRIXEQUATION_H__

#include <stdint.h> 				// Required for data type "uint8_t"
#include <stdlib.h> 				// Required for function "exit()" and dynamic memory allocation
#include <string.h>					// Required for function "memcpy"
#include <math.h>					// Required for function "pow"
#include <stdarg.h>					// Required for variadic functionality

// Error codes (Must exclude 0)
#define ERRMATSIZE		1
#define ERRMATOPR		2
#define ERREQUAL		3
#define ERRNOTOKEN		4
#define ERRARGOUT		5
#define ERRBRACKET		6
#define ERRUNARY		7
#define ERRARGCARDNEQ	8
#define ERRNODEPTR		9
#define ERRTKNUNKNOWN	10 		// Token code unknown
#define ERRRHSNOTRSLVD	11 		// R.H.S. of equal sign is not resolved
#define ERRINVSING		12

// MEQ types
typedef double			meqNum_t; 		// Type meqNumType determines the accuracy of the calculations
typedef meqNum_t		*meqMat_t; 		// Meq matrix type
typedef int8_t			meqCDim_t;		// Type meqCDim_t determines the number of rows and columns a vector or matrix can have. Needs to be signed.
typedef char			*meqStr_t;		// String type
typedef int16_t			meqStrInx_t;	// String index type determines the number of characters a string can have. Needs to be signed.
typedef uint8_t			meqBl_t;		// Boolean type
typedef uint8_t			meqCArg_t;		// Type meqCArg_t determines the number of input arguments an equation can have.

#define MEQTRUE 		((meqBl_t)1) 	// 'if (MEQTRUE)' should execute if statement
#define MEQFALSE 		((meqBl_t)0) 	// 'if (MEQFALSE)' should not execute if statement

/// Library
void Meq(meqStr_t eq, ...);
void MeqDispMat(meqMat_t mat, meqCDim_t cRow, meqCDim_t cCol, char* strMatName);

//#define MEQVERBOSE
//#define MEQDEBUG

#endif