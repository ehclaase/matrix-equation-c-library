/**
Author		: 	Etienne H. Claase
Date  		: 	20171201
Precedence	:	Level 0
						"="			Equal non-unary operator
				Level 1
						"(" ")"		Brackets
				Level 2
						"'"			Transpose
				Level 3
						"+"			Positive sign unary operator (Assumed if no sign included)
						"-"			Negative sign unary operator
				Level 4
						"*"			Multiplication non-unary operator
						".*"		Element-wise multiplication non-unary operator
						"/"			RHS devide non-unary operator
						"./"		RHS element-wise devide non-unary operator
						"\\"		LHS devide non-unary operator
						".\\"		LHS element-wise devide non-unary operator
						".x"		Cross-product non-unary operator (Only implemented for 3D column vectors)
						".."		Dot-product non-unary operator (Only implemented for column vectors)
				Level 5		
						"+"			Addition non-unary operator
						"-"			Addition non-unary operator
Constants	: 	pi 	-	The scalar constant Pi
				e 	-	The scalar natural exponent
				E[cRow,cCol] 	-	The rectangular identity matrix with 1's along its diagonal and zeros otherwise, where cRow is the matrix row cardinal, cCol is the matrix column cardinal.
Variables	: 	Variables contains the following syntax
					VarName[cRow,cCol,iArgIn]				
				where VarName may be any name for the matrix variable, excluding spaces, operators or brackets, 
				cRow is the matrix row cardinal, cCol is the matrix column cardinal,
				iArgIn is the function Meq input argument index corresponding to the matrix variable, starting at 0 after the equation string.
Implement	:	The global structure MeqStrc is used to store information about the equation (See MeqStrc_t definition and comments). 
				The equation string is partitioned into tokens and token types with codes by the parser MeqParser, and stored in MeqStrc. 
				Additional info about the tokens are also stored in MeqStrc, such as size, operator strings, and pointers to sub-results.
				Sub-results are stored in dynamically created matrices, using linked lists (See MeqMatInit, MeqMatAdd and MeqMatTerm).
				After the equation string is parsed, the interpreter uses the defined order of precedence to resolve the terms on the RHS of the equal sign to one term.
				Thereafter, the one term is coppied to the input argument corresponding to the term on the LHS of the equal sign.
				When the equation has been resolved, the memory of the sub-result matrices, as well as memory allocated in MeqStrc, are freed.
Licence 	:	This program is free software: you can redistribute it and/or modify
				it under the terms of the GNU General Public License as published by
				the Free Software Foundation, either version 3 of the License, or
				(at your option) any later version.

				This program is distributed in the hope that it will be useful,
				but WITHOUT ANY WARRANTY; without even the implied warranty of
				MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
				GNU General Public License for more details.

				You should have received a copy of the GNU General Public License
				along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "MatrixEquation.h"


/// Function definitions
void MeqTerm(void);


/// Preamble
#define MEQFOR(MeqForVar,MeqForBgn,MeqForEnd,MeqForInc)	for (MeqForVar=MeqForBgn;(((MeqForVar<=MeqForEnd) && (MeqForInc > 0)) || ((MeqForVar>=MeqForEnd) && (MeqForInc < 0)));MeqForVar+=MeqForInc)	
	
#ifdef MEQVERBOSE 
	#include <stdio.h>
	#define MeqVerbose(meqVerbose) meqVerbose
#else
	#define MeqVerbose(meqVerbose)
#endif

#ifdef MEQDEBUG 
	#include <stdio.h>
	#define MeqDebug(meqDebug) meqDebug
#else
	#define MeqDebug(meqDebug)
#endif

#define MeqFreeContainedDAMat(meqExpr){\
			MeqMatSaveHead();\
			meqExpr;\
			MeqMatRmvAboveSavedHead();\
		}

#define MeqExit(meqPreExitExpr,meqExitCode){\
			MeqDebug(meqPreExitExpr;\
					 MeqTerm();\
					 while(1);)\
			MeqTerm();\
			exit(meqExitCode);\
		}
	
//	Matrix equation string codes (Each code must be different, and not used for equation writing)
#define MEQCODE_VOID	((char)0xFF) 	// Character already used
#define MEQCODE_NUOPR	((char)0xFE) 	// Non-unary operator
#define MEQCODE_UOPR	((char)0xFD)	// Unary operator
#define MEQCODE_CNST	((char)0xFC) 	// Constant
#define MEQCODE_NUM		((char)0xFB) 	// Number
#define MEQCODE_ARG		((char)0xFA) 	// Input argument
#define MEQCODE_SRSLT	((char)0xF9) 	// Sub-result
#define MEQCODE_BRKOPN	((char)0xF8) 	// Open bracket
#define MEQCODE_BRKCLS	((char)0xF7) 	// Close bracket

// Constants
#define MEQ_PI			3.14159265358979
#define MEQ_EXP			2.71828182845905

// Structure used for matrix equation string (function Meq) parser and interpreter
typedef struct {
	meqStrInx_t		cEq;			// Equation string length
	meqStrInx_t		cArg;			// Input argument cardinal
	meqStr_t		eq;				// Equation string
	meqStr_t		eqCode;			// Coded equation string
	meqStr_t		*arrNuOprStr;	// Array containing non-unary operator strings
	meqStr_t		*arrUOprStr;	// Array containing unary operator strings
	meqStrInx_t		*arrArgInInx;		// Array indicating the input argument index
	meqCDim_t		*arrMatCRow;	// Array indicating the input argument or sub-result matrix row cardinals
	meqCDim_t		*arrMatCCol;	// Array indicating the input argument or sub-result matrix column cardinals
	meqMat_t		*arrMat;		// Array containing pointers to the input argument or sub-result matrix
	va_list			pVarArgIn;		// Pointer to variadic arguments of Meq
} MeqStrc_t;
MeqStrc_t MeqStrc;

// Structure used for dynamically allocating matrix memory
//		If only base node exists, all fields are NULL and 0.
//		The head node only contains pointer to previous node.
//		The node before the head node, contains the pointer to the last allocated matrix memory and the corresponding matrix sizes
//		Functions like a stack (last in, first out)
typedef struct MeqNode {
	struct MeqNode	*pNodePrev;
	meqMat_t		mat;
	meqCDim_t 		cRow;
	meqCDim_t 		cCol;	
} MeqNode_t;
MeqNode_t *pMeqNode = NULL;
MeqNode_t *pMeqNodeSavedHead = NULL; 


/// Memory management
// Create matrix base node
void MeqMatInit(void)
{
	if (pMeqNode == NULL){
		pMeqNode = (MeqNode_t *)(malloc(sizeof(MeqNode_t)));
		pMeqNode->pNodePrev = NULL;
		pMeqNode->mat 		= NULL;
		pMeqNode->cRow 		= 0;
		pMeqNode->cCol 		= 0;
	}
}
// Add matrix to current node and create new node
meqMat_t MeqMatAdd(meqCDim_t cRow, meqCDim_t cCol)
{
	MeqNode_t *pNodePrev;
	
	if ((cRow <= 0) || (cCol <= 0)){
		MeqExit(printf("Error in MeqMatAdd: Function defined for matrix dimensions >0."),ERRMATSIZE);
	}

	MeqMatInit();

	pMeqNode->mat = (meqMat_t)(malloc(cRow*cCol*sizeof(meqNum_t)));
	pMeqNode->cRow = cRow;
	pMeqNode->cCol = cCol;
	pNodePrev = pMeqNode;
	pMeqNode = (MeqNode_t *)(malloc(sizeof(MeqNode_t)));
	pMeqNode->pNodePrev = pNodePrev;
	pMeqNode->mat 		= NULL;
	pMeqNode->cRow 		= 0;
	pMeqNode->cCol 		= 0;
	
	return (pMeqNode->pNodePrev)->mat;	// Matrix is stored in previous node
}
// Free dynamically allocated memory for head of nodes and free head of nodes
void MeqMatRmvHead(void)
{
	MeqNode_t *pNodePrev;
	
	// Test if no nodes exist
	if (pMeqNode == NULL){
		// Do nothing
	}
	// Test if only initialised base node exists
	else if (pMeqNode->pNodePrev == NULL){
		free(pMeqNode);
		pMeqNode = NULL;
	}	
	// Case nodes more than one	
	else{
		pNodePrev = pMeqNode->pNodePrev;
		free(pMeqNode);
		pMeqNode = pNodePrev;
		free(pMeqNode->mat);
		pMeqNode->mat 	= NULL;
		pMeqNode->cRow 	= 0;
		pMeqNode->cCol	= 0;
	}
}
// Free all dynamically allocated memory for matrices
void MeqMatTerm(void)
{	
	while (pMeqNode != NULL) MeqMatRmvHead();
}
// Save the current head of nodes to global pointer
void MeqMatSaveHead(void)
{	
	pMeqNodeSavedHead = pMeqNode;
}
// Get the current head of nodes
MeqNode_t *MeqMatGetHead(void)
{	
	return pMeqNode;
}
// Get the last saved head of nodes
MeqNode_t *MeqMatGetSavedHead(void)
{	
	return pMeqNodeSavedHead;
}
// Free dynamically allocated memory for matrices of all nodes above the specified node
void MeqMatRmvAboveSpc(MeqNode_t *pMeqNodeSpc)
{	
	if (pMeqNodeSpc == NULL){
		MeqMatTerm();
	}else if (pMeqNode == NULL){
		if (pMeqNodeSpc != NULL) MeqExit(printf("Error in MeqMatRmvAboveSpc: Function undefined for (pMeqNode == NULL) and (pMeqNodeSpc != NULL)."),ERRNODEPTR);
	}else{
		// Free dynamically allocated memory
		while (pMeqNodeSpc != pMeqNode){
			MeqMatRmvHead();
			if (pMeqNode == NULL) MeqExit(printf("Error in MeqMatRmvAboveSpc: pMeqNodeSpc was not included in nodes."),ERRNODEPTR);
		}
	}
}
// Free dynamically allocated memory for matrices of all nodes above the last saved head of nodes
void MeqMatRmvAboveSavedHead(void)
{
	MeqMatRmvAboveSpc(MeqMatGetSavedHead());
}
// Initialise the matrix equation string structure
void MeqStrcInit(meqStr_t eq)
{
	meqStrInx_t iAux;
	
	// Equation string length
	MeqStrc.cEq = strlen(eq); 	// Does not include string termination character '\0'
	
	// Allocate memory to MEQ struct
	MeqStrc.eq			= (meqStr_t)malloc((MeqStrc.cEq+1)*sizeof(char));	// +1 to include EOL '\0' 
	MeqStrc.eqCode		= (meqStr_t)malloc(MeqStrc.cEq*sizeof(char));
	MeqStrc.arrNuOprStr	= (meqStr_t*)malloc(MeqStrc.cEq*sizeof(meqStr_t));
	MeqStrc.arrUOprStr = (meqStr_t*)malloc(MeqStrc.cEq*sizeof(meqStr_t));
	MeqStrc.arrArgInInx	= (meqStrInx_t*)malloc(MeqStrc.cEq*sizeof(meqStrInx_t));
	MeqStrc.arrMatCRow	= (meqCDim_t*)malloc(MeqStrc.cEq*sizeof(meqCDim_t));
	MeqStrc.arrMatCCol	= (meqCDim_t*)malloc(MeqStrc.cEq*sizeof(meqCDim_t));	
	MeqStrc.arrMat	= (meqNum_t**)malloc(MeqStrc.cEq*sizeof(meqNum_t*));
	
	// Initialise memory
	MEQFOR(iAux,0,MeqStrc.cEq-1,1){ 	// cEq-1 since index starts at 0, giving total of cEq elements
		MeqStrc.eq[iAux] = NULL;
		MeqStrc.eqCode[iAux] = NULL;
		MeqStrc.arrNuOprStr[iAux] = NULL;
		MeqStrc.arrUOprStr[iAux] = NULL;
		MeqStrc.arrArgInInx[iAux] = 0;
		MeqStrc.arrMatCRow[iAux] = 0;
		MeqStrc.arrMatCCol[iAux] = 0;
		MeqStrc.arrMat[iAux] = NULL;
	}
	MeqStrc.eq[MeqStrc.cEq] = NULL; 	// Include the termination element
}
// Terminate the matrix equation string structure
void MeqStrcTerm(void)
{
	meqStrInx_t iAux;
	
	// Free dynamically allocated memory
	free(MeqStrc.eq);
	free(MeqStrc.eqCode);
	MEQFOR(iAux,0,MeqStrc.cEq-1,1)	free(MeqStrc.arrNuOprStr[iAux]); 	// cEq-1 since index starts at 0, giving total of cEq elements
	free(MeqStrc.arrNuOprStr);
	MEQFOR(iAux,0,MeqStrc.cEq-1,1)	free(MeqStrc.arrUOprStr[iAux]); 	// cEq-1 since index starts at 0, giving total of cEq elements
	free(MeqStrc.arrUOprStr);
	free(MeqStrc.arrArgInInx);
	free(MeqStrc.arrMatCRow);
	free(MeqStrc.arrMatCCol);
	free(MeqStrc.arrMat);	// Note that the sub-result matrix memory is freed via MeqMatTerm, and the input argument memory is not freed.
}
// Free all memory, which were dynamically allocated for library functions.
void MeqTerm(void)
{
	MeqMatTerm();
	MeqStrcTerm();
}

/// Basic matrix math operations

// Matrix Scale
void MeqMatScl(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matIn, meqCDim_t cRowIn, meqCDim_t cColIn, meqNum_t scal)
{
	meqCDim_t 	iRow, iCol;
	
	*pCRowOut = cRowIn;
	*pCColOut = cColIn;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
	
	MEQFOR(iRow,0,*pCRowOut-1,1){
		MEQFOR(iCol,0,*pCColOut-1,1){
			(*pMatOut)[iRow*(*pCColOut)+iCol] = scal*matIn[iRow*(*pCColOut)+iCol];
		}
	}
	
}

// Matrix transpose "'"
void MeqMatTrsp(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matIn, meqCDim_t cRowIn, meqCDim_t cColIn)
{
	meqCDim_t 	iRowOut, iColOut;
	
	*pCRowOut = cColIn;
	*pCColOut = cRowIn;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
	
	MEQFOR(iRowOut,0,*pCRowOut-1,1){
		MEQFOR(iColOut,0,*pCColOut-1,1){
			(*pMatOut)[iRowOut*(*pCColOut)+iColOut] = matIn[iColOut*(*pCRowOut)+iRowOut];
		}
	}	
}

// Matrix addition "+" and subtraction "-"
void MeqMatAddSub(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR, meqBl_t blSub)
{
	meqCDim_t 	iRow, iCol, iMat;
	
	if ((cRowInL != cRowInR) && (cColInL != cColInR)) MeqExit(printf("Error in MeqMatMult: matInL[%d,%d] +- matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
	
	*pCRowOut = cRowInL;
	*pCColOut = cColInL;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
	
	MEQFOR(iRow,0,*pCRowOut-1,1){
		MEQFOR(iCol,0,*pCColOut-1,1){
			iMat = iRow*(*pCColOut)+iCol;
			if (blSub) 	(*pMatOut)[iMat] = matInL[iMat] - matInR[iMat];
			else 		(*pMatOut)[iMat] = matInL[iMat] + matInR[iMat];
		}
	}	
}

// Matrix multiplication "*"
void MeqMatMult(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqCDim_t 	iRowOut, iColOut, iAux, iMatOut;
	
	if (cColInL != cRowInR) MeqExit(printf("Error in MeqMatMult: matInL[%d,%d] * matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
	
	*pCRowOut = cRowInL;
	*pCColOut = cColInR;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
	
	// Mlt
	MEQFOR(iRowOut,0,*pCRowOut-1,1){
		MEQFOR(iColOut,0,*pCColOut-1,1){
			iMatOut = iRowOut*(*pCColOut) + iColOut;
			(*pMatOut)[iMatOut] = 0.0;
			MEQFOR(iAux,0,cColInL-1,1){
				(*pMatOut)[iMatOut] += matInL[iRowOut*cColInL + iAux]*matInR[iAux*cColInR + iColOut];
			}
		}
	}
}

// Matrix element-wise multiplication ".*"
void MeqMatMultElem(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqCDim_t 	iRow, iCol, iMat;
	
	if ((cRowInL == 1) && (cColInL == 1) && (cRowInR == 1) && (cColInR == 1)){
		*pCRowOut = 1;
		*pCColOut = 1;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		(*pMatOut)[0] = matInL[0]*matInR[0];		
	}else if ((cRowInL == 1) && (cColInL == 1)){
		MeqMatScl(pMatOut, pCRowOut, pCColOut, matInR, cRowInR, cColInR, matInL[0]);		
	}else if ((cRowInR == 1) && (cColInR == 1)){
		MeqMatScl(pMatOut, pCRowOut, pCColOut, matInL, cRowInL, cColInL, matInR[0]);
	}else{
		if ((cRowInL != cRowInR) || (cColInL != cColInR)) MeqExit(printf("Error in MeqMatMultElem: matInL[%d,%d] .* matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
		
		*pCRowOut = cRowInL;
		*pCColOut = cColInL;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		
		MEQFOR(iRow,0,*pCRowOut-1,1){
			MEQFOR(iCol,0,*pCColOut-1,1){
				iMat = iRow*(*pCColOut) + iCol;
				(*pMatOut)[iMat] = matInL[iMat]*matInR[iMat];
			}
		}		
	}
}

// Inverse of square matrix
// 		Gauss-Jordon elimination
void MeqMatInv(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matIn, meqCDim_t cRowIn, meqCDim_t cColIn)
{
	meqCDim_t 	iMatGet,iMatSet,iRow,iCol,iMax,iRowInv,iHold;
	meqMat_t 	matInAux;
	meqMat_t 	matOutAux;
	meqMat_t 	matIndx; 		// Used instead of having to swop rows after each row iteration
	meqNum_t 	scal;
	
	if (cRowIn != cColIn) MeqExit(printf("Error in MeqInv: matIn dimensions spec [%d,%d] not square.",cRowIn,cColIn),ERRMATSIZE); 

	*pCRowOut = cRowIn;
	*pCColOut = cColIn;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);	
		
	// Generate auxhilary matrices
	matInAux = MeqMatAdd(cRowIn, cColIn);
	matOutAux = MeqMatAdd(*pCRowOut, *pCColOut);
	matIndx = MeqMatAdd(cRowIn, 1);		
	
	// Initialise auxhiliray matrices: matInAux set to matIn; matOutAux set to identity matrix; matIndx set to {0,1,2,...,(cRowIn-1)}
	MEQFOR(iRow,0,cRowIn-1,1){
		MEQFOR(iCol,0,cColIn-1,1){
			iMatSet = iMatGet = iRow*cColIn + iCol;
			matInAux[iMatSet] = matIn[iMatGet];
			if (iRow == iCol) 	matOutAux[iMatSet] = 1.0;
			else 				matOutAux[iMatSet] = 0.0;
		}
		matIndx[iRow] = (meqNum_t)iRow;
	}		
	
	// Scale and subtract rows in matInAux, such that the identity matrix is obtained, whilst performing the same operations on matOutAux. The resulting matOutAux is the inverse.
	MEQFOR(iRowInv,0,cRowIn-1,1){			
		
		// Condition matrix by scaling remaining rows to a maximum row value of 1			
		MEQFOR(iRow,iRowInv,cRowIn-1,1){
			scal = 0.0;
			MEQFOR(iCol,0,cColIn-1,1){
				iMatGet = matIndx[iRow]*cColIn + iCol;					
				if (((scal >= 0.0) && ((matInAux[iMatGet]>scal) || (matInAux[iMatGet]<-scal))) || ((scal <= 0.0) && ((matInAux[iMatGet]>-scal) || (matInAux[iMatGet]<scal)))) 	scal = matInAux[iMatGet];					
			}
			if (scal == 0.0) MeqExit(printf("Error in MeqInv: matIn row with index %d is zeros; matIn not invertable.",matIndx[iRow]),ERRINVSING);
			MEQFOR(iCol,0,cColIn-1,1){
				iMatSet = matIndx[iRow]*cColIn + iCol;
				matInAux[iMatSet] /= scal;
				matOutAux[iMatSet] /= scal;
			}
		}	
	
		// Find the index corresponding to the maximum remaining element in column iRowInv
		scal = 0.0;
		iMax = -1;
		MEQFOR(iRow,iRowInv,cRowIn-1,1){
			iMatGet = matIndx[iRow]*cColIn + iRowInv;
			if (((scal >= 0.0) && ((matInAux[iMatGet]>scal) || (matInAux[iMatGet]<-scal))) || ((scal <= 0.0) && ((matInAux[iMatGet]>-scal) || (matInAux[iMatGet]<scal)))){
				scal = matInAux[iMatGet];
				iMax = iRow;
			}
		}			
		if (scal == 0.0) MeqExit(printf("Error in MeqInv: matIn column with index %d is zeros; matIn not invertable.",iRowInv),ERRINVSING);

		// Swop indeces
		iHold = matIndx[iRowInv];
		matIndx[iRowInv] = matIndx[iMax];
		matIndx[iMax] = iHold;	
	
		// Zero column iRowInv elements in matInAux except for row with index matIndx[iRowInv]
		MEQFOR(iRow,0,cRowIn-1,1){
			if (iRow != matIndx[iRowInv]){
									
				iMatGet = iRow*cColIn + iRowInv;
				scal = matInAux[iMatGet];
				iMatGet = matIndx[iRowInv]*cColIn + iRowInv;
				scal /= matInAux[iMatGet];		// DIV0 safety included by using maximum element in column iRowInv that is not zero
				
				if (scal != 0.0){	// If zero, then no subtraction necessary
					MEQFOR(iCol,0,cColIn-1,1){
						iMatSet = iRow*cColIn + iCol;
						iMatGet = matIndx[iRowInv]*cColIn + iCol;
						matInAux[iMatSet] -= scal*matInAux[iMatGet];
						matOutAux[iMatSet] -= scal*matOutAux[iMatGet];							
					}
				}					
			}
		}
	}
	
	// Set output
	MEQFOR(iRow,0,cRowIn-1,1){
		iMatGet = matIndx[iRow]*cColIn + iRow;
		scal = matInAux[iMatGet];	// matInAux is diagonal but not necessarily the identity matrix
		MEQFOR(iCol,0,cColIn-1,1){
			iMatSet = iRow*cColIn + iCol;
			iMatGet = matIndx[iRow]*cColIn + iCol;	
			(*pMatOut)[iMatSet] = matOutAux[iMatGet]/scal;
		}
	}
}

// Matrix pre-division "\\"
void MeqMatPreDiv(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqMat_t 	matAux;	
	meqCDim_t 	cRowAux,cColAux;
	
	if (cColInL != cRowInR) MeqExit(printf("Error in MeqMatPreDiv: matInL[%d,%d] \ matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
	
	*pCRowOut = cRowInL;
	*pCColOut = cColInR;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
	
	MeqMatInv(&matAux, &cRowAux, &cColAux, matInL, cRowInL, cColInL);
	
	MeqMatMult(pMatOut, pCRowOut, pCColOut, matAux, cRowAux, cColAux, matInR, cRowInR, cColInR);	
}

// Matrix element-wise pre-division ".\\"
void MeqMatPreDivElem(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqCDim_t 	iRow, iCol, iMat;
	
	if ((cRowInL == 1) && (cColInL == 1) && (cRowInR == 1) && (cColInR == 1)){
		*pCRowOut = 1;
		*pCColOut = 1;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		(*pMatOut)[0] = matInR[0]/matInL[0];		
	}else if ((cRowInL == 1) && (cColInL == 1)){
		MeqMatScl(pMatOut, pCRowOut, pCColOut, matInR, cRowInR, cColInR, 1.0/matInL[0]);		
	}else if ((cRowInR == 1) && (cColInR == 1)){
		*pCRowOut = cRowInL;
		*pCColOut = cColInL;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		
		MEQFOR(iRow,0,*pCRowOut-1,1){
			MEQFOR(iCol,0,*pCColOut-1,1){
				iMat = iRow*(*pCColOut) + iCol;
				(*pMatOut)[iMat] = matInR[0]/matInL[iMat];
			}
		}
	}else{
		if ((cRowInL != cRowInR) || (cColInL != cColInR)) MeqExit(printf("Error in MeqMatMultElem: matInL[%d,%d] .\\ matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
		
		*pCRowOut = cRowInL;
		*pCColOut = cColInL;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		
		MEQFOR(iRow,0,*pCRowOut-1,1){
			MEQFOR(iCol,0,*pCColOut-1,1){
				iMat = iRow*(*pCColOut) + iCol;
				(*pMatOut)[iMat] = matInR[iMat]/matInL[iMat];
			}
		}
	}
}

// Matrix post-division "/"
void MeqMatPostDiv(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqMat_t 	matAux;	
	meqCDim_t 	cRowAux,cColAux;
	
	if (cColInL != cRowInR) MeqExit(printf("Error in MeqMatPostDiv: matInL[%d,%d] / matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
	
	*pCRowOut = cRowInL;
	*pCColOut = cColInR;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
	
	MeqMatInv(&matAux, &cRowAux, &cColAux, matInR, cRowInR, cColInR);
	MeqMatMult(pMatOut, pCRowOut, pCColOut, matInL, cRowInL, cColInL, matAux, cRowAux, cColAux);	
}

// Matrix element-wise post-division "./"
void MeqMatPostDivElem(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqCDim_t 	iRow, iCol, iMat;
	
	if ((cRowInL == 1) && (cColInL == 1) && (cRowInR == 1) && (cColInR == 1)){
		*pCRowOut = 1;
		*pCColOut = 1;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		(*pMatOut)[0] = matInL[0]/matInR[0];		
	}else if ((cRowInL == 1) && (cColInL == 1)){
		*pCRowOut = cRowInR;
		*pCColOut = cColInR;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		
		MEQFOR(iRow,0,*pCRowOut-1,1){
			MEQFOR(iCol,0,*pCColOut-1,1){
				iMat = iRow*(*pCColOut) + iCol;
				(*pMatOut)[iMat] = matInL[0]/matInR[iMat];
			}
		}		
	}else if ((cRowInR == 1) && (cColInR == 1)){		
		MeqMatScl(pMatOut, pCRowOut, pCColOut, matInL, cRowInL, cColInL, 1.0/matInR[0]);
	}else{
		if ((cRowInL != cRowInR) || (cColInL != cColInR)) MeqExit(printf("Error in MeqMatMultElem: matInL[%d,%d] ./ matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
		
		*pCRowOut = cRowInL;
		*pCColOut = cColInL;
		*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);
		
		MEQFOR(iRow,0,*pCRowOut-1,1){
			MEQFOR(iCol,0,*pCColOut-1,1){
				iMat = iRow*(*pCColOut) + iCol;
				(*pMatOut)[iMat] = matInL[iMat]/matInR[iMat];
			}
		}
	}
}

// Matrix vector-product ".x"
void MeqMatCross(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	if ((cRowInL != 3) || (cRowInR != 3) || (cColInL != 1) || (cColInR != 1)) MeqExit(printf("Error in MeqMatCross: matInL[%d,%d] .x matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
	
	*pCRowOut = 3;
	*pCColOut = 1;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);		

	// Cross-product
	(*pMatOut)[0] = -matInL[2]*matInR[1] + matInL[1]*matInR[2];
	(*pMatOut)[1] =  matInL[2]*matInR[0] - matInL[0]*matInR[2];
	(*pMatOut)[2] = -matInL[1]*matInR[0] + matInL[0]*matInR[1];		
}	

// Matrix vector-product ".."
void MeqMatDot(meqMat_t* pMatOut, meqCDim_t* pCRowOut, meqCDim_t* pCColOut, meqMat_t matInL, meqCDim_t cRowInL, meqCDim_t cColInL, meqMat_t matInR, meqCDim_t cRowInR, meqCDim_t cColInR)
{
	meqCDim_t iAux;
	
	if ((cRowInL != cRowInR) || (cColInL != 1) || (cColInR != 1)) MeqExit(printf("Error in MeqMatDot: matInL[%d,%d] .. matInR[%d,%d] has incompatable sizes.",cRowInL,cColInL,cRowInR,cColInR),ERRMATSIZE);
	
	*pCRowOut = 1;
	*pCColOut = 1;
	*pMatOut = MeqMatAdd(*pCRowOut, *pCColOut);			

	// Dot-product
	(*pMatOut)[0] = 0.0;
	MEQFOR(iAux,0,cRowInL-1,1) (*pMatOut)[0] += matInL[iAux]*matInR[iAux];	
}


/// Display Functions
void MeqDispMat(meqMat_t mat, meqCDim_t cRow, meqCDim_t cCol, char* strMatName)
{
	meqCDim_t iRow,iCol;
	
	printf("%s = [\n",strMatName);
	for (iRow=0; iRow<cRow; iRow++){
		for (iCol=0; iCol<cCol; iCol++){
			printf("%f ",mat[iRow*cCol + iCol]);
		}
		printf("\n");
	}
	printf("]\n");
}


/// Error functions
void MeqErrMsg(meqStr_t errMsg, meqStr_t errStr, meqStrInx_t iErr)
{
	meqStrInx_t	iAux;
	
	printf("%s\n\n",errMsg);
	printf("%s\n",errStr);
	MEQFOR(iAux,0,iErr-1,1) printf(" ");
	printf("^\n\n");
}


/// Parser
// Parser helper functions
meqBl_t MeqIsChrNum(char chrIn)
{
	if ((chrIn>=48) && (chrIn<=57)) return MEQTRUE;		// ASCII[48,57] = DEC[0,9]
	else							return MEQFALSE;
}
meqNum_t MeqChrToNum(char chrIn)
{
	return (meqNum_t)(chrIn - 48);
}
meqBl_t MeqIsChrName(char chrIn)	// Note that numbers can also be included in names, but their inclusion into names are handled in MeqParser.
{
	if (((chrIn>=65) && (chrIn<=90)) ||		// Capitals
		((chrIn>=97) && (chrIn<=122)) ||	// Lowercase
		(chrIn == '_')){					// Underscore
			return MEQTRUE;
	}else	return MEQFALSE;
}

// Parser token functions
// The following functions defines the token types of an equation. 
// If the equation contains a part that is not identifiable via these functions, an error will be thrown.
// These functions only searches from the start index onwards.
meqBl_t MeqTknNuOpr(meqStrInx_t iBgn, meqStrInx_t *iEnd)		// Return true if next operator is non-unary (operator with two operands) and return false otherwise
{
	meqStrInx_t	iAux;
	
	if ((MeqStrc.eqCode[iBgn] == '+')	|| 
	    (MeqStrc.eqCode[iBgn] == '-')	||
		(MeqStrc.eqCode[iBgn] == '*')	||
		(MeqStrc.eqCode[iBgn] == '/')	||
		(MeqStrc.eqCode[iBgn] == '\\') || 
		(MeqStrc.eqCode[iBgn] == '=')){		
		
		// Check if '+' or '-' are unary operators
		if ((MeqStrc.eqCode[iBgn] == '+')	|| 
			(MeqStrc.eqCode[iBgn] == '-')){
			
			// Assumed that characters in eqCode prior to iBgn has been coded
			// Find previous token			
			iAux = iBgn;
			while(1){
				if (iAux == 0) break; 	
				iAux--;
				if (MeqStrc.eqCode[iAux] != MEQCODE_VOID) break;
			}			
			if ((MeqStrc.eqCode[iAux] == MEQCODE_VOID) 	|| 
			    (MeqStrc.eqCode[iAux] == MEQCODE_NUOPR) || 
				(MeqStrc.eqCode[iAux] == MEQCODE_UOPR) 	|| 
				(MeqStrc.eqCode[iAux] == MEQCODE_BRKOPN)){
				return MEQFALSE;				
			}
		}		

		// Allocate memory for the operator string
		MeqStrc.arrNuOprStr[iBgn] = (meqStr_t)malloc(2);
		MeqStrc.arrNuOprStr[iBgn][0] = MeqStrc.eqCode[iBgn];
		MeqStrc.arrNuOprStr[iBgn][1] = '\0';
		MeqStrc.eqCode[iBgn] = MEQCODE_NUOPR;

		// Set output
		*iEnd = iBgn;		

		return MEQTRUE;
	}
	else if (MeqStrc.eqCode[iBgn] == '.'){
		if (iBgn < MeqStrc.cEq){
			if ((MeqStrc.eqCode[iBgn+1] == '*')		|| 
				(MeqStrc.eqCode[iBgn+1] == '/')		||
				(MeqStrc.eqCode[iBgn+1] == '\\')	||
				(MeqStrc.eqCode[iBgn+1] == 'x')		||
				(MeqStrc.eqCode[iBgn+1] == '.')){					

					// Allocate memory for the operator string
					MeqStrc.arrNuOprStr[iBgn] = (meqStr_t)malloc(3);
					MeqStrc.arrNuOprStr[iBgn][0] = MeqStrc.eqCode[iBgn];
					MeqStrc.arrNuOprStr[iBgn][1] = MeqStrc.eqCode[iBgn+1];
					MeqStrc.arrNuOprStr[iBgn][2] = '\0';
					MeqStrc.eqCode[iBgn] = MEQCODE_NUOPR;
					MeqStrc.eqCode[iBgn+1] = MEQCODE_VOID;

					// Set output
					*iEnd = iBgn+1;

					return MEQTRUE;
			}
		}		
	}
	return MEQFALSE;
}
meqBl_t MeqTknUOpr(meqStrInx_t iBgn, meqStrInx_t *iEnd)	// Unary operator (operator with only one operand)
{
	meqStrInx_t	iAux;
	
	if (MeqStrc.eqCode[iBgn] == '\''){

		// Allocate memory for the operator string
		MeqStrc.arrUOprStr[iBgn] = (meqStr_t)malloc(2);
		MeqStrc.arrUOprStr[iBgn][0] = MeqStrc.eqCode[iBgn];
		MeqStrc.arrUOprStr[iBgn][1] = '\0';
		MeqStrc.eqCode[iBgn] = MEQCODE_UOPR;

		*iEnd = iBgn;

		return MEQTRUE;
	}
	
	// Check if '+' or '-' are unary operators
	if ((MeqStrc.eqCode[iBgn] == '+')	|| 
		(MeqStrc.eqCode[iBgn] == '-')){
		
		// Assumed that characters in eqCode prior to iBgn has been coded
		// Find previous token			
		iAux = iBgn;
		while(1){
			if (iAux == 0) break; 	
			iAux--;
			if (MeqStrc.eqCode[iAux] != MEQCODE_VOID) break;
		}		
		
		if ((MeqStrc.eqCode[iAux] == MEQCODE_VOID) 	|| 
		    (MeqStrc.eqCode[iAux] == MEQCODE_NUOPR) || 
			(MeqStrc.eqCode[iAux] == MEQCODE_UOPR) 	|| 
			(MeqStrc.eqCode[iAux] == MEQCODE_BRKOPN)){
			
			// Allocate memory for the operator string
			MeqStrc.arrUOprStr[iBgn] = (meqStr_t)malloc(2);
			MeqStrc.arrUOprStr[iBgn][0] = MeqStrc.eqCode[iBgn];
			MeqStrc.arrUOprStr[iBgn][1] = '\0';
			MeqStrc.eqCode[iBgn] = MEQCODE_UOPR;

			*iEnd = iBgn;

			return MEQTRUE;				
		}		
	}

	return MEQFALSE;
}
meqBl_t MeqTknCnst(meqStrInx_t iBgn, meqStrInx_t *iEnd)
{
	meqStrInx_t iSrch;
	meqStr_t 	strAux;	
	meqCDim_t 	cRow, cCol;
	meqCDim_t 	iRow, iCol;
	
	// Identity matrix "E[cRow,cCol]"
	if ((iBgn <= (MeqStrc.cEq-6)) &&  				// Require at lease 6 characters for identity matrix specification
	    (MeqStrc.eqCode[iBgn] == 'E') && 
		(MeqStrc.eqCode[iBgn+1] == '[')){
		iSrch = iBgn+1; 	// iSrch points to '['			
		
		// After '[' there must be a number
		if ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){				
			iSrch++;
			cRow = (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
			while ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){ 
				iSrch++;
				cRow *= 10;
				cRow += (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
			}
		}
		else return MEQFALSE;

		// After number there must be a ','
		if ((iSrch+1 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == ',')) iSrch++;
		else return MEQFALSE;

		// After ',' there must be a number
		if ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){				
			iSrch++;
			cCol = (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
			while ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){ 
				iSrch++;
				cCol *= 10;
				cCol += (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
			}
		}
		else return MEQFALSE;

		// After number there must be a ']'
		if ((iSrch+1 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == ']')) iSrch++;
		else return MEQFALSE;

		*iEnd = iSrch;
		
		// Set Structure fields
		MeqStrc.arrMatCRow[iBgn] = cRow;
		MeqStrc.arrMatCCol[iBgn] = cCol;
		//MeqStrc.arrMat[iBgn] = (meqMat_t)(malloc(cRow*cCol*sizeof(meqNum_t)));
		MeqStrc.arrMat[iBgn] = MeqMatAdd(cRow, cCol);
		
		// Set identity matrix
		MEQFOR(iRow,0,cRow-1,1){
			MEQFOR(iCol,0,cCol-1,1){
				if 	(iRow == iCol) 	MeqStrc.arrMat[iBgn][iRow*cCol+iCol] = 1.0;
				else 				MeqStrc.arrMat[iBgn][iRow*cCol+iCol] = 0.0;
			}
		}		
		MeqStrc.eqCode[iBgn] = MEQCODE_CNST;
		MEQFOR(iSrch,iBgn+1,*iEnd,1) MeqStrc.eqCode[iSrch] = MEQCODE_VOID;

		return MEQTRUE;
	}
	// Scalar constants
	else if (MeqIsChrName(MeqStrc.eqCode[iBgn])){
		iSrch = iBgn;
		while( (iSrch+1 < MeqStrc.cEq) && ( (MeqIsChrName(MeqStrc.eqCode[iSrch+1])) || (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]) ) ) ){
			iSrch++;
		}
		// Constant
		if ((iSrch == MeqStrc.cEq) || (MeqStrc.eqCode[iSrch+1] != '[')){
			*iEnd = iSrch;
			
			// Store constant value as matrix
			strAux = (meqStr_t)malloc((*iEnd-iBgn+1)+1);
			MEQFOR(iSrch,iBgn,*iEnd,1) strAux[iSrch-iBgn] = MeqStrc.eqCode[iSrch];
			strAux[*iEnd+1-iBgn] = '\0';			
			if ((strcmp(strAux,"pi") == 0) || 
			    (strcmp(strAux,"e") == 0)){
				MeqStrc.arrMatCRow[iBgn] = 1;
				MeqStrc.arrMatCCol[iBgn] = 1;
				//MeqStrc.arrMat[iBgn] = (meqMat_t)(malloc(sizeof(meqNum_t)));
				MeqStrc.arrMat[iBgn] = MeqMatAdd(1, 1);				
				if 		(strcmp(strAux,"pi") == 0)	MeqStrc.arrMat[iBgn][0] = MEQ_PI;
				else if (strcmp(strAux,"e") == 0)	MeqStrc.arrMat[iBgn][0] = MEQ_EXP;
			}else{
				return MEQFALSE;
			}
			free(strAux);
			MeqStrc.eqCode[iBgn] = MEQCODE_CNST;
			MEQFOR(iSrch,iBgn+1,*iEnd,1) MeqStrc.eqCode[iSrch] = MEQCODE_VOID;

			return MEQTRUE;
		}
	}

	return MEQFALSE;

}
meqBl_t MeqTknNum(meqStrInx_t iBgn, meqStrInx_t *iEnd)
{
	meqBl_t blDot = MEQFALSE;
	meqStrInx_t iDot = 0;
	meqStrInx_t iSrch,oOrdr;
	meqNum_t scl;
	meqNum_t ordr;

	if (MeqIsChrNum(MeqStrc.eqCode[iBgn])){	// If the token starts with a number, it is a number
		// Find end of number token
		iSrch = iBgn;

		while ( ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))) ||
			    ((iSrch+2 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == '.') && (MeqIsChrNum(MeqStrc.eqCode[iSrch+2]))) ){ // Number decimal point must contain a digit after the point
					if (MeqStrc.eqCode[iSrch+1] == '.'){
						if (blDot) break;
						blDot = 1;
						iDot = iSrch+1;
					}
					iSrch++;
		}
		if (!(blDot)) iDot = iSrch+1;

		// Convert string token to number
		*iEnd = iSrch;
		MeqStrc.arrMatCRow[iBgn] = 1;
		MeqStrc.arrMatCCol[iBgn] = 1;
		//MeqStrc.arrMat[iBgn] = (meqMat_t)(malloc(sizeof(meqNum_t)));
		MeqStrc.arrMat[iBgn] = MeqMatAdd(1, 1);
		MeqStrc.arrMat[iBgn][0] = 0.0;
		MEQFOR(iSrch,iBgn,*iEnd,1){
			if (iSrch != iDot){
				scl = MeqChrToNum(MeqStrc.eqCode[iSrch]);
				if (iDot > iSrch){	
					ordr = 1.0; 
					MEQFOR(oOrdr,1,(meqNum_t)iDot - (meqNum_t)iSrch - 1.0,1) ordr *= 10.0;
					MeqStrc.arrMat[iBgn][0] += scl*ordr;
				}else{
					ordr = 1.0; 
					MEQFOR(oOrdr,-1,(meqNum_t)iDot - (meqNum_t)iSrch,-1) ordr *= 0.1;
					MeqStrc.arrMat[iBgn][0] += scl*ordr;
				}
			}
			// Modify equation code
			if (iSrch != iBgn) MeqStrc.eqCode[iSrch] = MEQCODE_VOID;
		}
		// Modify equation code
		MeqStrc.eqCode[iBgn] = MEQCODE_NUM;

		return MEQTRUE;
	}

	return MEQFALSE;
}
meqBl_t MeqTknArg(meqStrInx_t iBgn, meqStrInx_t *iEnd)
{
	meqStrInx_t iSrch;
	meqCDim_t cRow, cCol;
	meqStrInx_t	iArg;

	iSrch = iBgn;
	if (MeqIsChrName(MeqStrc.eqCode[iBgn])){
		iSrch = iBgn;
		while( (iSrch+1 < MeqStrc.cEq) && ( (MeqIsChrName(MeqStrc.eqCode[iSrch+1])) || (MeqIsChrNum(MeqStrc.eqCode[iSrch+1])) ) ){
			iSrch++;
		}
		// Input argument (e.g. "name[1,2,3]")
		if ((iSrch+7) < MeqStrc.cEq){		// Requires at least 7 characters to index an input argument

			// After name there must be a '['
			if ((iSrch+1 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == '[')) iSrch++;
			else return MEQFALSE;
			
			// After '[' there must be a number
			if ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){				
				iSrch++;
				cRow = (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
				while ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){ 
					iSrch++;
					cRow *= 10;
					cRow += (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
				}
			}
			else return MEQFALSE;

			// After number there must be a ','
			if ((iSrch+1 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == ',')) iSrch++;
			else return MEQFALSE;

			// After ',' there must be a number
			if ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){				
				iSrch++;
				cCol = (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
				while ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){ 
					iSrch++;
					cCol *= 10;
					cCol += (meqCDim_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
				}
			}
			else return MEQFALSE;

			// After number there must be a ','
			if ((iSrch+1 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == ',')) iSrch++;
			else return MEQFALSE;

			// After ',' there must be a number
			if ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){				
				iSrch++;
				iArg = (meqStrInx_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
				while ((iSrch+1 < MeqStrc.cEq) && (MeqIsChrNum(MeqStrc.eqCode[iSrch+1]))){ 
					iSrch++;
					iArg *= 10;
					iArg += (meqStrInx_t)MeqChrToNum(MeqStrc.eqCode[iSrch]);
				}
			}
			else return MEQFALSE;

			// After number there must be a ']'
			if ((iSrch+1 < MeqStrc.cEq) && (MeqStrc.eqCode[iSrch+1] == ']')) iSrch++;
			else return MEQFALSE;

			*iEnd = iSrch;

			// Set parser values
			MeqStrc.arrMatCRow[iBgn] = cRow;
			MeqStrc.arrMatCCol[iBgn] = cCol;
			MeqStrc.arrArgInInx[iBgn] = iArg;
			MeqStrc.eqCode[iBgn] = MEQCODE_ARG;
			MEQFOR(iSrch,iBgn+1,*iEnd,1) MeqStrc.eqCode[iSrch] = MEQCODE_VOID;

			return MEQTRUE;
		}
	}

	return MEQFALSE;

}
meqBl_t MeqTknBrk(meqStrInx_t iBgn, meqStrInx_t *iEnd)
{
	if (MeqStrc.eqCode[iBgn] == '('){
		MeqStrc.eqCode[iBgn] = MEQCODE_BRKOPN;
		*iEnd = iBgn;
		return MEQTRUE;
	}
	if (MeqStrc.eqCode[iBgn] == ')'){
		MeqStrc.eqCode[iBgn] = MEQCODE_BRKCLS;
		*iEnd = iBgn;
		return MEQTRUE;
	}

	return MEQFALSE;
}

// Parser main function
// The parser uses the parser token functions to identify the equation tokens from left to right.
// The input argument cardinal is returned.
void MeqParser(meqStr_t eq)
{
	meqStrInx_t		iAux,iArg,cArg,iRow,iCol;
	meqBl_t			blTknArg;
	meqMat_t		meqArg;

	// Copy eq string to code string before parsing
	memcpy(MeqStrc.eq,eq,MeqStrc.cEq+1);		// +1 used to include \0
	memcpy(MeqStrc.eqCode,eq,MeqStrc.cEq);

	// Mark spaces as used
	MEQFOR(iAux,0,MeqStrc.cEq-1,1){	
		if (MeqStrc.eqCode[iAux] == ' '){
			MeqStrc.eqCode[iAux] = MEQCODE_VOID;
		}
	}

	// Scan through equation and replace characters with codes
	iAux = 0;
	cArg = 0;
	while (iAux<MeqStrc.cEq){
		if (MeqStrc.eqCode[iAux] != MEQCODE_VOID){
			if ((!(MeqTknNuOpr(iAux, &iAux)))	&&
				(!(MeqTknUOpr(iAux, &iAux)))	&&
				(!(MeqTknCnst(iAux, &iAux)))	&&
				(!(MeqTknNum(iAux, &iAux)))		&&
				(!(MeqTknArg(iAux, &iAux)))		&&
				(!(MeqTknBrk(iAux, &iAux)))){		// Token could not be identified
					MeqExit(MeqErrMsg("Error in MeqParser: Could not identify equation token.", MeqStrc.eq, iAux),ERRTKNUNKNOWN);
			}
		}
		iAux++;
	}

	// Get input argument cardinal (determined with maximum input argument index)
	cArg = 0;
	MEQFOR(iAux,0,MeqStrc.cEq-1,1){				// Scan through equation code
		if (MeqStrc.eqCode[iAux] == MEQCODE_ARG){	// Look for input argument
			if (MeqStrc.arrArgInInx[iAux] + 1 > cArg) cArg = MeqStrc.arrArgInInx[iAux] + 1;
		}
	}
	MeqStrc.cArg = cArg;
	
	// Get pointers to input arguments
	iArg = 0;
	MEQFOR(iArg,0,MeqStrc.cArg-1,1){
		meqArg = va_arg(MeqStrc.pVarArgIn,meqMat_t);
		MEQFOR(iAux,0,MeqStrc.cEq-1,1){
			if ((MeqStrc.eqCode[iAux] == MEQCODE_ARG) && (MeqStrc.arrArgInInx[iAux] == iArg)){
				MeqStrc.arrMat[iAux] = meqArg;
			}
		}
	}
	
	// Display equation parsing
	MeqVerbose(
		printf("MeqParser:\n\n");
		printf("%s\n\n",eq);
		MEQFOR(iAux,0,MeqStrc.cEq-1,1){
			printf("%c	:	Code = 0x%x ",eq[iAux], MeqStrc.eqCode[iAux]);
			if (MeqStrc.eqCode[iAux] == MEQCODE_VOID){
				printf("(Void)");
			}
			else if (MeqStrc.eqCode[iAux] == MEQCODE_NUOPR){
				printf("(Non-unary operator \"%s\")",MeqStrc.arrNuOprStr[iAux]);
			}
			else if (MeqStrc.eqCode[iAux] == MEQCODE_UOPR){
				printf("(Unary operator = \"%s\")",MeqStrc.arrUOprStr[iAux]);
			}
			else if (MeqStrc.eqCode[iAux] == MEQCODE_NUM){
				printf("(Number [%d,%d] = [ ",MeqStrc.arrMatCRow[iAux],MeqStrc.arrMatCCol[iAux]);
			}
			else if (MeqStrc.eqCode[iAux] == MEQCODE_CNST){
				printf("(Constant [%d,%d] = [ ",MeqStrc.arrMatCRow[iAux],MeqStrc.arrMatCCol[iAux]);
			}
			else if (MeqStrc.eqCode[iAux] == MEQCODE_ARG){
				printf("(Input argument [%d,%d,%d] = [ ",MeqStrc.arrMatCRow[iAux],MeqStrc.arrMatCCol[iAux],MeqStrc.arrArgInInx[iAux]);				
			}
			if ((MeqStrc.eqCode[iAux] == MEQCODE_NUM) || 
				(MeqStrc.eqCode[iAux] == MEQCODE_CNST) ||
				(MeqStrc.eqCode[iAux] == MEQCODE_ARG)){
				MEQFOR(iRow,0,MeqStrc.arrMatCRow[iAux]-1,1){
					MEQFOR(iCol,0,MeqStrc.arrMatCCol[iAux]-1,1){
						printf("%f ",MeqStrc.arrMat[iAux][iRow*MeqStrc.arrMatCCol[iAux]+iCol]);
					}
					if (iRow < MeqStrc.arrMatCRow[iAux]-1) printf("; ");
					else printf("]");
				}
				printf(")");
			}
			printf("\n");
		}
		printf("\nInput argument cardinal: %d\n\n",MeqStrc.cArg);
	)

}


/// Interpreter
// Interpreter helper functions
meqBl_t MeqIsStrInStr(char* strTkn, char* strSubj, int iStrSubjBgn)
{
	meqStrInx_t iAux;

	iAux = 0;
	while (strTkn[iAux] != '\0'){
		if ((strSubj[iStrSubjBgn+iAux] == '\0') || (strTkn[iAux] != strSubj[iStrSubjBgn+iAux])){
			return MEQFALSE;
		}
		iAux++;
	}
	return MEQTRUE;
}
meqBl_t MeqTknNxt(meqStrInx_t iWinLhs, meqStrInx_t iWinRhs, meqStrInx_t *iTknNxt)
{
	meqStrInx_t iAux;
	
	MEQFOR(iAux,iWinLhs,iWinRhs,1){
		if (MeqStrc.eqCode[iAux] != MEQCODE_VOID){
			*iTknNxt = iAux;
			return MEQTRUE;
		}
	}
	return MEQFALSE;
}
meqBl_t MeqTknSpcNxt(meqStrInx_t iWinLhs, meqStrInx_t iWinRhs, char tknSpec, meqStrInx_t *iTknNxt)
{
	meqStrInx_t iAux;
	
	MEQFOR(iAux,iWinLhs,iWinRhs,1){
		if (MeqStrc.eqCode[iAux] == tknSpec){ 
			*iTknNxt = iAux;
			return MEQTRUE;
		}		
	}
	return MEQFALSE;
}
meqBl_t MeqTknPrv(meqStrInx_t iWinLhs, meqStrInx_t iWinRhs, meqStrInx_t *iTknPrev)
{
	meqStrInx_t iAux;
	MEQFOR(iAux,iWinRhs,iWinLhs,-1){
		if (MeqStrc.eqCode[iAux] != MEQCODE_VOID){ 
			*iTknPrev = iAux;
			return MEQTRUE;
		}		
	}
	return MEQFALSE;
}
meqBl_t MeqTknSpcPrv(meqStrInx_t iWinLhs, meqStrInx_t iWinRhs, char tknSpec, meqStrInx_t *iTknPrev)
{
	meqStrInx_t iAux;
	
	MEQFOR(iAux,iWinRhs,iWinLhs,-1){
		if (MeqStrc.eqCode[iAux] == tknSpec){ 
			*iTknPrev = iAux;
			return MEQTRUE;
		}		
	}
	return MEQFALSE;
}
meqBl_t MeqIsTknMat(char tknCode)
{
	if ((tknCode == MEQCODE_CNST) || (tknCode == MEQCODE_NUM) || (tknCode == MEQCODE_ARG) || (tknCode == MEQCODE_SRSLT)) return MEQTRUE;
	return MEQFALSE;
}
void MeqDispEqCode(void)
{
	meqStrInx_t iAux;
	static meqBl_t blDispHdr = MEQTRUE;
	
	if (blDispHdr){
		blDispHdr = MEQFALSE;
		printf("Interpreter: Matrix Equation Progress\n");
		printf("N - Non-unary operator\n");
		printf("U - Unary operator\n");
		printf("C - Constant\n");
		printf("V - Number\n");
		printf("A - Input argument\n");
		printf("S - Sub-result\n\n");
	}
	
	MEQFOR(iAux,0,MeqStrc.cEq-1,1){
		if 			(MeqStrc.eqCode[iAux] == MEQCODE_VOID){
			printf(" ");
		}else if	(MeqStrc.eqCode[iAux] == MEQCODE_BRKOPN){
			printf("(");
		}else if	(MeqStrc.eqCode[iAux] == MEQCODE_BRKCLS){
			printf(")");
		}else if 	(MeqStrc.eqCode[iAux] == MEQCODE_NUOPR){
			printf("N");
		}else if 	(MeqStrc.eqCode[iAux] == MEQCODE_UOPR){
			printf("U");
		}else if 	(MeqStrc.eqCode[iAux] == MEQCODE_CNST){
			printf("C");
		}else if 	(MeqStrc.eqCode[iAux] == MEQCODE_NUM){
			printf("V");
		}else if 	(MeqStrc.eqCode[iAux] == MEQCODE_ARG){
			printf("A");
		}else if 	(MeqStrc.eqCode[iAux] == MEQCODE_SRSLT){
			printf("S");
		}
	}
	printf("\n");
}

// Interpreter main function
void MeqInterp(void)
{
	meqStrInx_t iAux,iEql,iWinLhs,iWinRhs,iOpr,iRslt,iOprndLhs,iOprndRhs;
	meqBl_t 	blBrk,blOpr;	

	// Check if the first equation token is an argument.	
	if (MeqTknNxt(0, MeqStrc.cEq-1, &iRslt) == 0) MeqExit(MeqErrMsg("Error in MeqInterp: Could not find any token.", MeqStrc.eq, 0),ERRNOTOKEN);
	if (MeqStrc.eqCode[iRslt] != MEQCODE_ARG){ 
		MeqExit(MeqErrMsg("Error in MeqInterp: First equation token is not an input argument.", MeqStrc.eq, iRslt),ERRARGOUT);	
	}
	
	// Check if the second equation token is an equal sign
	iAux = iRslt;
	if (MeqTknNxt(iAux+1, MeqStrc.cEq-1, &iAux) == 0) MeqExit(MeqErrMsg("Error in MeqInterp: Could not find any token after the first argument.", MeqStrc.eq, iRslt),ERRNOTOKEN);
	if ((MeqStrc.eqCode[iAux] != MEQCODE_NUOPR) || (strcmp(MeqStrc.arrNuOprStr[iAux],"=") != 0)){ 
		MeqExit(MeqErrMsg("Error in MeqInterp: Second equation token is not an equal sign.", MeqStrc.eq, iAux),ERREQUAL);
	}	
	iEql = iAux;
	
	// Step through order of precedence to determine the next operation
	// 		The operation is performed within the case statement
	// 		Continue this process until RHS of equality is resolved
	MeqVerbose(MeqDispEqCode();)
	while (1){
	
		// Initialise Interpreter helper variables		
		iWinLhs = iEql+1; 					// Search window begin
		iWinRhs = MeqStrc.cEq - 1; 			// Search window end	(cardinal-1 gives index)
		blOpr = MEQFALSE; 					// Operator found 		(used to determine if the RHS of equality has been resolved)		
		blBrk = MEQFALSE; 					// Brackets found		(used to determine if the RHS of equality has been resolved; used to determine if redundant brackets needs to be removed)		
		
		switch(1){ 	// Start with precedence level 1, and use the switch-case-statement cascading property when break is not called to move to the next precedence level
			case 1: // Brackets (Level 1 precedence): Set the search window within the inner most LHS bracket pair. If there are no brackets, leave search window inchanged from initial value.
				// Find first closing bracket
				if (MeqTknSpcNxt(iWinLhs, iWinRhs, MEQCODE_BRKCLS, &iAux)){
					blBrk = MEQTRUE;
					iWinRhs = iAux-1;
					// Find matching opening bracket
					if (MeqTknSpcPrv(iWinLhs, iWinRhs, MEQCODE_BRKOPN, &iAux)){
						iWinLhs = iAux+1;
					}
					else{						
						MeqExit(MeqErrMsg("Error in MeqInterp: Could not find matching opening bracket.", MeqStrc.eq, iWinRhs+1),ERRBRACKET);
					}
				}						
			case 2:	// Transpose (Level 2 precedence)				
				// Look for transpose operator
				iAux = iWinLhs-1;
				while (MeqTknSpcNxt(iAux+1, iWinRhs, MEQCODE_UOPR, &iAux)){
					if (strcmp(MeqStrc.arrUOprStr[iAux],"'") == 0){
						blOpr = MEQTRUE;
						break;
					}					
				}
				
				if (blOpr){
					iOpr = iAux;

					// Find operand on LHS of transpose
					if (MeqTknPrv(iWinLhs, iOpr-1, &iAux)){
						if (MeqIsTknMat(MeqStrc.eqCode[iAux])){
							iOprndLhs = iAux;
							
							// Do operation: The operation replaces the operand with the sub-result						
							MeqMatTrsp(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs]);							

							// Modify codes
							MeqStrc.eqCode[iOpr] = MEQCODE_SRSLT;
							MeqStrc.eqCode[iOprndLhs] = MEQCODE_VOID;
							
							break;
						}
					}
					MeqExit(MeqErrMsg("Error in interpreter MeqInterp: Could not find operand on LHS of transpose.", MeqStrc.eq, iOpr),ERRNOTOKEN);
				}
			case 3:	// Unary + or - (Level 3 precedence)				
				// Look for +- unary operator
				iAux = iWinLhs-1;
				while (MeqTknSpcNxt(iAux+1, iWinRhs, MEQCODE_UOPR, &iAux)){
					if ((strcmp(MeqStrc.arrUOprStr[iAux],"+") == 0) || (strcmp(MeqStrc.arrUOprStr[iAux],"-") == 0)){ 
						blOpr = MEQTRUE;
						break;
					}					
				}
				
				if (blOpr){
					iOpr = iAux;

					// Find operand on RHS of unary +-
					if (MeqTknNxt(iOpr+1, iWinRhs, &iAux)){
						if (MeqIsTknMat(MeqStrc.eqCode[iAux])){
							iOprndRhs = iAux;
							
							// Do operation
							if (strcmp(MeqStrc.arrUOprStr[iOpr],"+") == 0){ 	// No operation needed. Simply place matrix with dimensions at operator index.
								MeqStrc.arrMat[iOpr] = MeqStrc.arrMat[iOprndRhs];
								MeqStrc.arrMatCRow[iOpr] = MeqStrc.arrMatCRow[iOprndRhs];
								MeqStrc.arrMatCCol[iOpr] = MeqStrc.arrMatCCol[iOprndRhs];
							}else{ 	// The operation replaces the operand with the sub-result. MeqStrc.arrMat[iOpr] is NULL.
								MeqMatScl(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs], -1.0);
							}

							// Modify codes
							MeqStrc.eqCode[iOpr] = MEQCODE_SRSLT;
							MeqStrc.eqCode[iOprndRhs] = MEQCODE_VOID;
							
							break;
						}
					}
					MeqExit(MeqErrMsg("Error in interpreter MeqInterp: Could not find operand on RHS of unary +-.", MeqStrc.eq, iOpr),ERRNOTOKEN);
				}
			case 4:	// Non-unary operators except + and - (Level 4 precedence)				
				// Look for non-unary operator
				iAux = iWinLhs-1;
				while (MeqTknSpcNxt(iAux+1, iWinRhs, MEQCODE_NUOPR, &iAux)){
					if ((strcmp(MeqStrc.arrNuOprStr[iAux],"+") != 0) && (strcmp(MeqStrc.arrNuOprStr[iAux],"-") != 0)){
						blOpr = MEQTRUE;
						break;
					}
				}				
			
				if (blOpr){
					iOpr = iAux;

					// Find operand on LHS of non-unary operator
					if (MeqTknPrv(iWinLhs, iOpr-1, &iAux)){
						if (MeqIsTknMat(MeqStrc.eqCode[iAux])){
							iOprndLhs = iAux;
							// Find operand on RHS of non-unary operator
							if (MeqTknNxt(iOpr+1, iWinRhs, &iAux)){
								if (MeqIsTknMat(MeqStrc.eqCode[iAux])){
									iOprndRhs = iAux;
									
									// Do operation.
									if (strcmp(MeqStrc.arrNuOprStr[iOpr],"*") == 0){
										MeqMatMult(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],".*") == 0){
										MeqMatMultElem(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],"\\") == 0){
										MeqMatPreDiv(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],".\\") == 0){
										MeqMatPreDivElem(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],"/") == 0){
										MeqMatPostDiv(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],"./") == 0){
										MeqMatPostDivElem(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],".x") == 0){
										MeqMatCross(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else if (strcmp(MeqStrc.arrNuOprStr[iOpr],"..") == 0){
										MeqMatDot(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs]);
									}else{
										MeqExit(printf("Error in MeqInterp: Operator %s not implemented.",MeqStrc.arrNuOprStr[iOpr]),ERRMATOPR);	// Invalid matrix operator
									}
									
									// Modify codes
									MeqStrc.eqCode[iOpr] = MEQCODE_SRSLT;
									MeqStrc.eqCode[iOprndLhs] = MEQCODE_VOID;
									MeqStrc.eqCode[iOprndRhs] = MEQCODE_VOID;
									
									break;
								}
							}
							MeqExit(MeqErrMsg("Error in interpreter MeqInterp: Could not find operands on RHS of non-unary non-sign operator.", MeqStrc.eq, iOprndRhs),ERRNOTOKEN);
						}
					}
					MeqExit(MeqErrMsg("Error in interpreter MeqInterp: Could not find operands on LHS of non-unary non-sign operator.", MeqStrc.eq, iOprndLhs),ERRNOTOKEN);
				}
			case 5:	// Non-unary + or - (Level 5 precedence)				
				// Look for non-unary operator
				iAux = iWinLhs-1;
				while (MeqTknSpcNxt(iAux+1, iWinRhs, MEQCODE_NUOPR, &iAux)){
					if ((strcmp(MeqStrc.arrNuOprStr[iAux],"+") == 0) || (strcmp(MeqStrc.arrNuOprStr[iAux],"-") == 0)){
						blOpr = MEQTRUE;
						break;
					}
				}				
			
				if (blOpr){
					iOpr = iAux;

					// Find operand on LHS of non-unary operator
					if (MeqTknPrv(iWinLhs, iOpr-1, &iAux)){
						if (MeqIsTknMat(MeqStrc.eqCode[iAux])){
							iOprndLhs = iAux;
							// Find operand on RHS of non-unary operator
							if (MeqTknNxt(iOpr+1, iWinRhs, &iAux)){
								if (MeqIsTknMat(MeqStrc.eqCode[iAux])){
									iOprndRhs = iAux;
									
									// Do operation. MeqStrc.arrMat[iOpr] is NULL.
									MeqMatAddSub(&(MeqStrc.arrMat[iOpr]), &(MeqStrc.arrMatCRow[iOpr]), &(MeqStrc.arrMatCCol[iOpr]), MeqStrc.arrMat[iOprndLhs], MeqStrc.arrMatCRow[iOprndLhs], MeqStrc.arrMatCCol[iOprndLhs], MeqStrc.arrMat[iOprndRhs], MeqStrc.arrMatCRow[iOprndRhs], MeqStrc.arrMatCCol[iOprndRhs], (meqBl_t)(MeqStrc.eq[iOpr] == '-'));
																		
									// Modify codes
									MeqStrc.eqCode[iOpr] = MEQCODE_SRSLT;
									MeqStrc.eqCode[iOprndLhs] = MEQCODE_VOID;
									MeqStrc.eqCode[iOprndRhs] = MEQCODE_VOID;
									
									break;
								}
							}
							MeqExit(MeqErrMsg("Error in interpreter MeqInterp: Could not find operands on RHS of non-unary non-sign operator.", MeqStrc.eq, iOprndRhs),ERRNOTOKEN);
						}
					}
					MeqExit(MeqErrMsg("Error in interpreter MeqInterp: Could not find operands on LHS of non-unary non-sign operator.", MeqStrc.eq, iOprndLhs),ERRNOTOKEN);
				}
			default: // Remove redundant brackets if no operator has been found. In the case that an operator is found, the operation is performed, and a break is called, such that this point is not reached.
				if (blBrk && (!blOpr)){
					MeqStrc.eqCode[iWinLhs-1] = MEQCODE_VOID;
					MeqStrc.eqCode[iWinRhs+1] = MEQCODE_VOID;
				}
				
		}	// switch		

		// Exit while loop if no operations or brackets remain
		if ((!blOpr) && (!blBrk)){
			break;
		}
		
		MeqVerbose(MeqDispEqCode();)
		
	} 	// while
	
	
	// Set output	
	 
	// Find RHS result index
	iWinLhs = iEql + 1;						// Search window begin
	iWinRhs = MeqStrc.cEq - 1; 				// Search window end	(cardinal-1 gives index)
	if (!MeqTknNxt(iWinLhs, iWinRhs, &iAux)) 	MeqExit(MeqErrMsg("Error in MeqInterp: Could not find token on R.H.S. of equal sign.", MeqStrc.eq, iEql),ERRNOTOKEN)
	
	// Check that there is only one token on R.H.S. of equal sign
	if (MeqTknNxt(iAux+1, iWinRhs, &iAux)) 		MeqExit(MeqErrMsg("Error in MeqInterp: Terms on R.H.S. of equal sign not resolved.", MeqStrc.eq, iEql),ERRRHSNOTRSLVD);
	// Check that token is a matrix
	if (!MeqIsTknMat(MeqStrc.eqCode[iAux])) 	MeqExit(MeqErrMsg("Error in MeqInterp: Result on R.H.S. of equal sign not a matrix.", MeqStrc.eq, iEql),ERRRHSNOTRSLVD);
	// Check that the result and output have correct sizes
	if ((MeqStrc.arrMatCRow[iRslt] != MeqStrc.arrMatCRow[iAux]) || (MeqStrc.arrMatCCol[iRslt] != MeqStrc.arrMatCCol[iAux])) MeqExit(MeqErrMsg("Error in MeqInterp:", MeqStrc.eq, iRslt); printf("Specified output dimensions [%d,%d] not equal to calculated dimensions [%d,%d].",MeqStrc.arrMatCRow[iRslt],MeqStrc.arrMatCCol[iRslt],MeqStrc.arrMatCRow[iAux],MeqStrc.arrMatCCol[iAux]);,ERRARGCARDNEQ);
	
	// Set output
	memcpy(MeqStrc.arrMat[iRslt],MeqStrc.arrMat[iAux],MeqStrc.arrMatCRow[iRslt]*MeqStrc.arrMatCCol[iRslt]*sizeof(meqNum_t));
	
}

// MEQ main function
void Meq(meqStr_t eq, ...)
{
	MeqFreeContainedDAMat(
		// Initialise memory
		MeqStrcInit(eq);

		// Initialise variadic argument list
		va_start(MeqStrc.pVarArgIn,eq);
		
		// Parser
		MeqParser(eq);
	
		// Interpreter (Use Parser structure)
		MeqInterp();
		
		// Terminate variadic argument list
		va_end(MeqStrc.pVarArgIn);
		
		// Free memory
		MeqStrcTerm();
	)
}

