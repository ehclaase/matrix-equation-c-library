#ifndef __MATRIXEQUATIONTEST_H__
#define __MATRIXEQUATIONTEST_H__

#include "MatrixEquation.h"
#include<math.h>

#define MEQTESTVERBOSE

// Test declaration
meqBl_t MeqTest(meqBl_t blVerbose);

#endif